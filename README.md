# CRUG API and Extensions

![CRUG logo](crug_skyline.png)

> R is highly extensible through the use of packages for specific functions and specific applications. 

## Repository for CRUG scripts and projects involving APIs and extensions including:

- ### Application interfaces (Adobe, Office, open-source)
- ### I/O handling (csv, json, xml)
- ### Networking connections (ftp, ssh)
- ### OS processing (Bash, CMD, PowerShell)
- ### Programming interfaces (C, C++, Java)
- ### Relational databases (Oracle, Postgres, SQL Server)

