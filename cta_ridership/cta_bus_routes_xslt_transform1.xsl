<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:k="http://www.opengis.net/kml/2.2">
    <xsl:output method="xml" omit-xml-declaration="no" indent="yes"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="/k:kml">
     <xsl:copy>
       <xsl:apply-templates select="descendant::k:description"/>
     </xsl:copy>
    </xsl:template>

    <xsl:template match="k:description">
     <xsl:copy>
       <xsl:value-of select="substring-before(substring-after(., '&lt;/head&gt;'), '&lt;/html&gt;')" disable-output-escaping="yes"/>
     </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
