DROP VIEW IF EXISTS cta_bus_ridership_view;
CREATE VIEW cta_bus_ridership_view AS
    SELECT 
        strftime('%Y-%m-%d', b.date * 86400, 'unixepoch') AS [date],
        b.route_number, r.route_number0, r.route_name,
        b.day_type, b.rides, 
        r.weekday_flag, r.saturday_flag, r.sunday_flag, r.shape_length
    FROM cta_bus_ridership b 
    LEFT JOIN cta_bus_routes r   
       ON b.route_number = r.route_number;


DROP VIEW IF EXISTS cta_rail_ridership_view;
CREATE VIEW cta_rail_ridership_view AS
    SELECT 
        strftime('%Y-%m-%d', r.date * 86400, 'unixepoch') AS [date], 
        r.station_id, r.station_name, r.day_type, r.rides,
        s.station_descriptive_name, s.ada_flag, 
        s.red_line, s.blue_line, s.green_line, 
        s.brown_line, s.purple_line, s.purple_express,
        s.yellow_line, s.pink_line, s.orange_line, 
        s.latitude, s.longitude
    FROM cta_rail_ridership r 
    LEFT JOIN cta_rail_stations s   
       ON r.station_id = s.station_id;


DROP VIEW IF EXISTS cta_station_rail_ridership_view;
CREATE VIEW cta_station_rail_ridership_view AS
	WITH sub AS ( 
		SELECT 
            strftime('%Y-%m-%d', r.date * 86400, 'unixepoch') AS [date], 
            r.station_id, s.station_name, r.rides, 
            s.red_line, s.blue_line, s.green_line,
            s.brown_line, s.purple_line, s.purple_express,
            s.yellow_line, s.pink_line, s.orange_line,
            r.rides / (
               (s.red_line + s.blue_line + s.green_line + 
                s.brown_line + s.purple_line + s.purple_express + 
                s.yellow_line + s.pink_line + s.orange_line) * 1.0
            ) AS station_rides
        FROM cta_rail_ridership r 
        LEFT JOIN cta_rail_stations s 
             ON r.station_id = s.station_id 
     )
	 SELECT [date], station_id, station_name, 'red line' AS line, rides, station_rides 
     FROM sub WHERE red_line = 1
	 UNION ALL
 	 SELECT [date], station_id, station_name, 'blue line' AS line, rides, station_rides 
     FROM sub WHERE blue_line = 1
	 UNION ALL
	 SELECT [date], station_id, station_name, 'green line' AS line, rides, station_rides 
     FROM sub WHERE green_line = 1
	 UNION ALL
	 SELECT [date], station_id, station_name, 'brown line' AS line, rides, station_rides 
     FROM sub WHERE brown_line = 1
	 UNION ALL
	 SELECT [date], station_id, station_name, 'purple line' AS line, rides, station_rides 
     FROM sub WHERE purple_line = 1
	 UNION ALL
	 SELECT [date], station_id, station_name, 'purple express' AS line, rides, station_rides 
     FROM sub WHERE purple_express = 1
	 UNION ALL
	 SELECT [date], station_id, station_name, 'yellow line' AS line, rides, station_rides
     FROM sub WHERE yellow_line = 1
	 UNION ALL
	 SELECT [date], station_id, station_name, 'pink line' AS line, rides, station_rides 
     FROM sub WHERE pink_line = 1
	 UNION ALL
	 SELECT [date], station_id, station_name, 'orange line' AS line, rides, station_rides 
     FROM sub WHERE orange_line = 1;


