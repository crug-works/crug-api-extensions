<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:k="http://www.opengis.net/kml/2.2">
    <xsl:output method="xml" omit-xml-declaration="no" indent="yes"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="/k:kml">
     <xsl:copy>
       <xsl:apply-templates select="k:description">
         <xsl:sort select="descendant::k:td[text()='ROUTE0']/following-sibling::k:td"/>
       </xsl:apply-templates>
     </xsl:copy>
    </xsl:template>

    <xsl:template match="k:description">
       <xsl:apply-templates select="descendant::k:table[2]"/>
    </xsl:template>

    <xsl:template match="k:table">
     <xsl:element name="DATA" namespace="http://www.opengis.net/kml/2.2">
       <xsl:apply-templates select="k:tr"/>
     </xsl:element>
    </xsl:template>

    <xsl:template match="k:tr">
      <xsl:element name="{k:td[1]}" namespace="http://www.opengis.net/kml/2.2">
        <xsl:value-of select="k:td[2]"/>
      </xsl:element>
    </xsl:template>

</xsl:stylesheet>
